package com.example.xz.appsigverif;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import java.io.ByteArrayInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;


public class MainActivity extends Activity {
    private static final String TAG = "MAIN_ACTIVITY";
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = getApplicationContext();

        pingCert();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void pingCert(){
        try {
            String hexMusicDigest = getAppCertificatePublicKeyFingerprint("com.android.music");
            String hexCalendarDigest = getAppCertificatePublicKeyFingerprint("com.example.xz.appsigverif");
            String message = String.format("Music: %s, self: %s", hexMusicDigest, hexCalendarDigest);
            Log.d(TAG, message);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public String getAppCertificatePublicKeyFingerprint(String packageName) throws
            PackageManager.NameNotFoundException, CertificateException,
            NoSuchAlgorithmException{
        PackageManager pm = context.getPackageManager();
        PackageInfo packageInfo = null;

        //Google should name this function as GET_SIGNING_CERT
        packageInfo = pm.getPackageInfo(packageName, PackageManager.GET_SIGNATURES);
        Signature[] signatures = packageInfo.signatures;

        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        //Android check the signature before returning the signing certificate.
        X509Certificate cert = (X509Certificate)cf.generateCertificate(new ByteArrayInputStream(
                signatures[0].toByteArray()
        ));

        byte[] certificateEncoded = cert.getEncoded();
        byte[] certificateFingerPrint = getMessageDigest("SHA256", certificateEncoded);
        Log.d(TAG, "cert fingerprint: " + byteArrayToHex(certificateFingerPrint));

        //We should ping the public key instead of the cert, because cert can be updated.
        PublicKey publicKey = cert.getPublicKey();
        byte[] publicKeyEncoding = publicKey.getEncoded();
        return byteArrayToHex(getMessageDigest("SHA256", publicKeyEncoding));
    }

    public byte[] getMessageDigest(String algorithm, byte[] message) throws NoSuchAlgorithmException{
        MessageDigest messageDigest = MessageDigest.getInstance(algorithm);
        messageDigest.update(message, 0, message.length);
        byte[] digest = messageDigest.digest();
        return digest;
    }


    public static String byteArrayToHex(byte[] a) {
        StringBuilder sb = new StringBuilder(a.length * 2);
        for(byte b: a)
            sb.append(String.format("%02x", b & 0xff));
        return sb.toString();
    }
}
